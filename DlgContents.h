﻿#ifndef ContentsDlg_H
#define ContentsDlg_H

#include <QDialog>
#include "common.h"
#include "QListWidgetSlide.h"
#include "ThreadCacheBook.h"

namespace Ui {
	class DlgContents;
}

class DlgContents : public DlgCustom
{
	Q_OBJECT
public:

	explicit DlgContents(QWidget* parent = nullptr);
	~DlgContents();
	void UpdateContents();
	void ListWidgetClear();

public:
signals:
	void on_sig_update_contents(bool b);

public slots:
	void on_slots_update_contents(bool b);
	void show();
	void reject();

private slots:
	void on_listWidgetContents_itemClicked(QListWidgetItem* item);
	void on_btnUpdateList_clicked();
	void on_cbSort_stateChanged(int arg1);

private:
	Ui::DlgContents* ui;
	Qt::CheckState check_state_old = Qt::CheckState::Unchecked;
};

#endif // ContentsDlg_H
