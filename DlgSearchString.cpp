﻿#include "DlgSearchString.h"
#include "ui_DlgSearchString.h"
#include "MainWindow.h"
#include <thread>


DlgSearchString::DlgSearchString(QWidget* parent) :
	DlgCustom(parent),
	ui(new Ui::DlgSearchString)
{
	ui->setupUi(this);
	ui->listWidgetSearchText->setWordWrap(true);
	int value = g_set->value("DialogSearchString/search_max_count", 100).toInt();
	ui->spinBoxMaxCount->setValue(value);
	connect(this, &DlgSearchString::on_sig_query_book_string, this, &DlgSearchString::on_slot_query_book_string);
}

DlgSearchString::~DlgSearchString()
{
	delete ui;
}

void DlgSearchString::on_btnSearchText_clicked()
{
	using namespace std;
	static QString strSearchText = ui->lineEdit->text();
	auto run = [&]()
		{
			m_threadQueryBookStringStop = false;
			auto iListCountMax = ui->spinBoxMaxCount->value();
			QString strBtnText = ui->btnSearchText->text();
			ui->btnSearchText->setText(strBtnText + BUTTON_IN_PROGRESS);

			auto& strListChapter = ((DlgBookRead*)parent())->m_chapterList;
			if (m_listBookInfo[0].IsWeb())
			{
				int iChapter = 0;
				int iListCount = 0;
				for (auto& chapter : strListChapter)
				{
					if (m_threadQueryBookStringStop == true) break;
					if (chapter.text.isEmpty())
					{
						QString strChapterFilePath = chapter.head;
						ModifySymbol(strChapterFilePath);
						QString strFilePathChapter = BOOK_LIST_SAVE_DIR"cache/" + m_listBookInfo[0].name;
						strFilePathChapter += '/' + strChapterFilePath + ".chapter";
						ifstream file(strFilePathChapter.toLocal8Bit(), ios::in | ios::binary);
						if (file.good())
						{
							string content((istreambuf_iterator<char>(file)),
								(istreambuf_iterator<char>()));
							chapter.text = QString::fromUtf8(content.c_str());
							file.close();
						}
					}
					QStringList list = chapter.GetHeadAndText().split("\n");//按行分割字符串
					for (int row = 0; row < list.size(); row++)
					{
						if (list[row].indexOf(strSearchText) != -1)
						{
							emit on_sig_query_book_string(iChapter, row, list[row]);
							iListCount++;
							if (iListCount >= iListCountMax || m_threadQueryBookStringStop == true)
							{
								goto EXIT_T;
							}
						}
					}
					iChapter++;
				}
			}
			else
			{
				int iChapter = 0;
				int iListCount = 0;
				for (auto& chapter : strListChapter)
				{
					if (m_threadQueryBookStringStop == true) goto EXIT_T;
					QStringList list = chapter.text.split("\n");//按行分割字符串
					for (int row = 0; row < list.size(); row++)
					{
						if (list[row].indexOf(strSearchText) != -1)
						{
							emit on_sig_query_book_string(iChapter, row, list[row]);
							iListCount++;
							if (iListCount >= iListCountMax || m_threadQueryBookStringStop == true)
							{
								goto EXIT_T;
							}
						}
					}
					iChapter++;
				}
			}

		EXIT_T:
			ui->btnSearchText->setText(strBtnText);
		};

	if (m_threadQueryBookString.joinable())
	{
		m_threadQueryBookStringStop = true;
		m_threadQueryBookString.join();
	}
	else
	{
		strSearchText = ui->lineEdit->text();
		if (strSearchText.isEmpty())
			return;
		ui->labelSearchCount->setText("0");
		ui->listWidgetSearchText->clear();
		m_threadQueryBookString = std::thread(run);
		m_threadQueryBookString.detach();
	}
}

void DlgSearchString::on_slot_query_book_string(int iChapter, int row, QString strRow)
{
	QListWidgetItem* aItem; //每一行是一个QListWidgetItem
	aItem = new QListWidgetItem(); //新建一个项
	QString strInfo = QString::number(iChapter) + " " + QString::number(row) + " " + strRow;
	aItem->setText(strInfo); //设置文字标签
	ui->listWidgetSearchText->addItem(aItem);
	int count = ui->labelSearchCount->text().toInt() + 1;
	ui->labelSearchCount->setText(QString::number(count));
	ui->listWidgetSearchText->update();
}

void DlgSearchString::on_listWidgetSearchText_currentTextChanged(const QString& currentText)
{
	int head = currentText.indexOf(" ");
	if (head == -1)
		return;
	m_listBookInfo[0].chapter = currentText.left(head).toInt();
	head++;
	int tail = currentText.indexOf(" ", head);
	m_listBookInfo[0].pos = currentText.mid(head, tail - head).toInt();
	((DlgBookRead*)parent())->UpdateChapter();
	hide();
}

void DlgSearchString::ListWidgetClear()
{
	ui->listWidgetSearchText->clear();
}

void DlgSearchString::on_spinBoxMaxCount_valueChanged(int arg1)
{
	g_set->setValue("DialogSearchString/search_max_count", arg1);
}
