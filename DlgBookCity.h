﻿#ifndef DIALOGBOOKCITY_H
#define DIALOGBOOKCITY_H

#include <QDialog>
#include <QNetworkAccessManager>
#include <QtNetwork>
#include <QTextCodec>
#include <QIcon>
#include "common.h"

namespace Ui {
	class DlgBookCity;
}

class DlgBookCity : public DlgCustom
{
	Q_OBJECT

public:
	explicit DlgBookCity(QWidget* parent = nullptr);
	~DlgBookCity();
	void getUrlText(QString strUrl);

public slots:
	void show();

private slots:

	void replyFinished(QNetworkReply*);

	void on_btnAddWebBook_clicked();

	void on_btnOpenUrl_clicked();

	void on_textBrowser_anchorClicked(const QUrl& arg1);

	void on_btnBackspace_clicked();

	void on_cbLineEditUrl_currentIndexChanged(const QString& arg1);

	void on_btnSearchWebBook_clicked();

	void on_btnCollectionUrl_clicked();

	void reject();

	void on_cbLineEditUrl_currentTextChanged(const QString& arg1);

private:
	Ui::DlgBookCity* ui;

	QNetworkRequest request;
	QNetworkAccessManager* manager;
	QStringList m_strUrlHistory;
	QString m_strText;
	bool m_isCollectionUpdate = false;
	QIcon m_icon_collection;
	QIcon m_icon_collection_disable;
	string m_search_prefix_url;
	bool m_utf8{ true };
};

#endif // DIALOGBOOKCITY_H
