#ifndef QLISTWIDGETSLIDE_H
#define QLISTWIDGETSLIDE_H

#include <QObject>
#include <QListWidget>
#include <QTimer>

class QListWidgetSlide : public QListWidget
{
	Q_OBJECT
public:
	QListWidgetSlide(QWidget* parent = Q_NULLPTR);
	virtual void timerEvent(QTimerEvent* event);
private:
	int m_nTimerID = 0;
	int m_slideSpeed = 0;//执行次数
	qint64 m_MillisecondTimeDifference = 0;

protected:
	void mousePressEvent(QMouseEvent* event);        //单击
	void mouseReleaseEvent(QMouseEvent* event);      //释放
	void mouseMoveEvent(QMouseEvent* event);         //移动

	//QPoint m_pos;
	QPoint m_point;
	int m_origin = 0;
	//bool m_bDrag;
};

#endif // QLISTWIDGETSLIDE_H
