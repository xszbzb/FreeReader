#ifndef WEB_WIDGET_H
#define WEB_WIDGET_H

#ifdef __ANDROID__
#define WEB_WIDGET_ENABLE 0
#endif

#if WEB_WIDGET_ENABLE

#include <QWidget>
#include <QtQuickWidgets/QQuickWidget>
#include <QUrl>
#include "dialogcustom.h"
#include "common.h"

namespace Ui {
	class DlgWebWidget;
}

class DlgWebWidget : public DialogCustom
{
	Q_OBJECT

public:
	explicit DlgWebWidget(QWidget* parent = nullptr);
	~DlgWebWidget();

	Q_INVOKABLE void setText(QString url);

signals:
	void sigGo(QUrl url);
	void sigReflash();
	void sigForward();
	void sigBack();

private slots:
	void on_btnBack_clicked();

	void on_btnRefresh_clicked();

	void on_btnForward_clicked();

	void on_btnGo_clicked();

	void on_btnClose_clicked();

private:
	Ui::DlgWebWidget* ui;
	QQuickWidget* m_pWebView;

};

#endif
#endif // WEB_WIDGET_H
