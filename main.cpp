﻿#include "MainWindow.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QRect>
#include <QFont>
#include <QVariant>
#include <QSettings>
#include "common.h"
#include <map>
#ifdef __ANDROID__
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QtAndroid>
#include <QtWebView>
#include <QQmlApplicationEngine>
#include <QQuickWidget>
#include <QtQuick/QQuickView>
#include <QtQml/QQmlContext>
#endif

deque<sBookInfo> m_listBookInfo;
std::mutex g_mtxListBookPosChange;
map<wstring, wstring> g_mapRegularFilter;
QSettings* g_set;
int g_get_web_repeat_times = GET_WEB_REPEAT_TIMES;
bool g_bAndroidForegroundRunning = true;
MainWindow* g_pMainWindow = nullptr;

int main(int argc, char* argv[])
{
#ifdef __ANDROID__
	requestPermission();
	//extern void registerNativeMethods();
	//registerNativeMethods();

	//申请通知权限
	QAndroidJniObject::callStaticMethod<void>(
		"qt/java/NotificationPermission",
		"enableNotificationAccess",
		"(Landroid/content/Context;)V",
		QtAndroid::androidContext().object());

#if 1
	//申请电池优化白名单
	bool bPermission = QAndroidJniObject::callStaticMethod<jboolean>(
		"qt/java/FcfrtAppBhUtils",
		"isIgnoringBatteryOptimizations",
		"(Landroid/content/Context;)V",
		QtAndroid::androidContext().object());
	if (!bPermission)
	{
		QAndroidJniObject::callStaticMethod<void>(
			"qt/java/FcfrtAppBhUtils",
			"requestIgnoreBatteryOptimizations",
			"(Landroid/content/Context;)V",
			QtAndroid::androidContext().object());
	}
#endif

#if WEB_WIDGET_ENABLE
	QtWebView::initialize();
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
#endif
#endif

	QString strFilePath = BOOK_LIST_SAVE_DIR"log/";
	QDir dir;
	dir.mkpath(strFilePath);
	static bool bStop = false;
	DelSuffixFile(BOOK_LIST_SAVE_DIR, "lock", bStop, 1);
	QSettings set(BOOK_LIST_SAVE_DIR"config.ini", QSettings::IniFormat);
	g_set = &set;
	log_open(&g_log, BOOK_LIST_SAVE_DIR"log/FreeReader.log", LOG_LEVEL_DEBUG, 3, 300000);
	LOG_DEBUG_P("main start");
	QApplication a(argc, argv);
	MainWindow w;
	g_pMainWindow = &w;
	w.show();
	int iRet = 0;
	iRet = a.exec();
	LOG_DEBUG_P("main end");
	log_close(&g_log);
	return iRet;
}
