#ifndef QWIDGETDRAG_H
#define QWIDGETDRAG_H

#include <QWidget>
#include <QMouseEvent>


class QWidgetDrag : public QWidget
{
	Q_OBJECT
public:
	QWidgetDrag(QWidget* parent = nullptr);

protected:
	virtual void mousePressEvent(QMouseEvent* event);
	virtual void mouseMoveEvent(QMouseEvent* event);
	virtual void mouseReleaseEvent(QMouseEvent* event);

private:
	bool m_move;
	QPoint m_startPoint;
	QPoint m_windowPoint;

signals:

public slots:
};

#endif // QWIDGETDRAG_H
