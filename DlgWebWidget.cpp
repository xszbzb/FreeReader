#include "DlgWebWidget.h"

#if WEB_WIDGET_ENABLE

#include "ui_WebWidget.h"
#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#include <QVBoxLayout>
#include <QTimer>

DlgWebWidget::DlgWebWidget(QWidget* parent) :
	DialogCustom(parent),
	ui(new Ui::DlgWebWidget)
{
	ui->setupUi(this);
	setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);
	//m_pWebView = new QQuickWidget(QUrl("qrc:/main.qml"));
	//m_pWebView->setSource(QUrl("qrc:/main.qml"));
	//m_pWebView->setResizeMode(QQuickWidget::SizeRootObjectToView);
	//m_pWebView->rootContext()->setContextProperty("widget", this);
	//QVBoxLayout* layout = new QVBoxLayout(ui->wgtWeb);
	//layout->addWidget(m_pWebView);

	ui->quickWidget->rootContext()->setContextProperty("widget", this);
	QTimer::singleShot(2000, this, [this]() {
		emit sigGo(QUrl::fromUserInput(ui->lineEdit->text()));
		});
	//this->hide();
}

DlgWebWidget::~DlgWebWidget()
{
	delete ui;
}

void DlgWebWidget::setText(QString url)
{
	ui->lineEdit->setText(url);
}

void DlgWebWidget::on_btnBack_clicked()
{
	emit sigBack();
}


void DlgWebWidget::on_btnRefresh_clicked()
{
	emit sigReflash();
}


void DlgWebWidget::on_btnForward_clicked()
{
	emit sigForward();
}


void DlgWebWidget::on_btnGo_clicked()
{
	emit sigGo(QUrl::fromUserInput(ui->lineEdit->text()));
}

void DlgWebWidget::on_btnClose_clicked()
{
	hide();
}

#endif
