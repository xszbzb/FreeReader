#include "DlgCustom.h"
#include <QCloseEvent>
#include "common.h"
#include "MainWindow.h"

DlgCustom::DlgCustom(QWidget* parent) : QDialog(parent)
{
	setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);
	connect(this, &DlgCustom::on_sig_close, this, &DlgCustom::reject);
}

void DlgCustom::closeEvent(QCloseEvent* event)
{
	//this->hide();//只是隐藏，不关闭[关闭之后，Matlab会消失]
	if (isVisible())
		emit on_sig_close();// reject();
	event->ignore();
}

void DlgCustom::reject() {
	if (isVisible())
		QDialog::reject();
}

//bool DialogCustom::event(QEvent* event)
//{
//	switch (event->type())
//	{
//	case 12:
//	case 77:
//		break;
//	default:
//		qDebug() << tr("event->type():%1").arg(event->type());
//	}
//
//	return QDialog::event(event);
//}

extern MainWindow* g_pMainWindow;

//bool DialogCustom::event(QEvent* event)
//{
//	switch (event->type())
//	{
//	case 12:
//	case 77:
//		break;
//	case QEvent::WindowActivate:
//		qDebug() << tr("event->type():WindowActivate:%1").arg(event->type());
//		if (g_pMainWindow)
//		{
//			g_pMainWindow->CallNativeOnResume();
//		}
//		break;
//	case QEvent::WindowDeactivate:
//		qDebug() << tr("event->type():WindowDeactivate:%1").arg(event->type());
//		if (g_pMainWindow)
//		{
//			g_pMainWindow->CallNativeOnPause();
//		}
//		break;
//	case QEvent::ActivationChange:
//		qDebug() << tr("event->type():ActivationChange:%1").arg(event->type());
//		break;
//	default:
//		qDebug() << tr("event->type():%1").arg(event->type());
//	}
//
//	return QDialog::event(event);
//}
