﻿#include "DlgAddBook.h"
#include "ui_DlgAddBook.h"
#include <QDir>
#include <QDirIterator>
#include "common.h"
#include <QComboBox>
#include <QDateTime>

DlgAddBook::DlgAddBook(QWidget* parent) :
	DlgCustom(parent),
	ui(new Ui::DlgAddBook)
{
	ui->setupUi(this);
	setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);
	ui->listWidget->clear();

#ifdef _WIN32
	ui->comboBox->setCurrentText("2");
#endif
#ifdef __ANDROID__
	ui->comboBox->setCurrentText("5");
#endif
}

DlgAddBook::~DlgAddBook()
{
	delete ui;
}

int DlgAddBook::FindFile(const QString& strFilePath, int layer)
{
	QDir dir(strFilePath);
	if (!dir.exists())
	{
		return -1;
	}
	dir.setFilter(QDir::Dirs | QDir::Files | QDir::NoSymLinks);
	dir.setSorting(QDir::DirsFirst);
	QFileInfoList list = dir.entryInfoList();

	int file_count = list.count();
	if (file_count <= 2)
	{
		return 0;
	}

	for (int i = 0; i < file_count; i++)
	{
		QFileInfo file_info = list.at(i);
		if (file_info.fileName()[0] == ".")
		{
			continue;
		}
		QString suffix = file_info.suffix();
		QString absolute_file_path = file_info.absoluteFilePath();

		if (file_info.isFile())
		{
			if (QString::compare(suffix, QString("txt"), Qt::CaseInsensitive) == 0)
			{
				QFileInfo info(absolute_file_path);
				if (info.exists() && info.size() > 100000)
				{
					m_strFilePathList.push_back(absolute_file_path);
				}
			}
		}
		else if (file_info.isDir() && layer > 0)
			FindFile(absolute_file_path, layer - 1);
	}
	return 0;
}

QStringList DlgAddBook::selectedFiles()
{
	QStringList strList;
	QList<QListWidgetItem*> list = ui->listWidget->selectedItems();
	for (QListWidgetItem* item : list)
	{
		QString str = item->text();
		//读取最后一行为文件路径
		str = str.right(str.length() - str.lastIndexOf("\n") - 1);
		strList.append(str);
	}
	return strList;
}

void DlgAddBook::on_btnFindBook_clicked()
{
	static QString strTextTraversalDdepth;
	QString str = ui->comboBox->currentText();
	if (strTextTraversalDdepth == str)
	{
		return;
	}
	strTextTraversalDdepth = str;
	ui->btnFindBook->setEnabled(false);
	QString curPath = "/";
#ifdef __ANDROID__
	curPath = ROOT_PATH;
#endif

	m_strFilePathList.clear();
	int layer = str.toInt();
	FindFile(curPath, layer);
	on_comboBoxSortOrder_currentTextChanged(ui->comboBoxSortOrder->currentText());
	ui->btnFindBook->setEnabled(true);
}

void DlgAddBook::on_checkBox_stateChanged(int arg1)
{
	if (arg1)
	{
		ui->listWidget->selectAll();
		ui->listWidget->hide();
		ui->listWidget->show();
	}
	else
	{
		ui->listWidget->clearSelection();
	}
}

bool CompareForTime(const QString& str, const QString& other)
{
	QFileInfo info(str);
	QDateTime dt = info.lastModified();
	QFileInfo infoOther(other);
	QDateTime dtOther = infoOther.lastModified();
	return dt.toMSecsSinceEpoch() < dtOther.toMSecsSinceEpoch();
}

bool CompareForSize(const QString& str, const QString& other)
{
	QFileInfo info(str);
	QFileInfo infoOther(other);
	return info.size() < infoOther.size();
}

bool CompareForName(const QString& str, const QString& other)
{
	QFileInfo info(str);
	QFileInfo infoOther(other);
	return info.fileName() > infoOther.fileName();
}

void DlgAddBook::on_comboBoxSortOrder_currentTextChanged(const QString& arg1)
{
	using CompareFunc = bool (*)(const QString& str, const QString& other);
	CompareFunc cf = CompareForTime;
	if (arg1 == tr("按时间"))
	{
		cf = CompareForTime;
	}
	else if (arg1 == tr("按大小"))
	{
		cf = CompareForSize;
	}
	else if (arg1 == tr("按名称"))
	{
		cf = CompareForName;
	}
	std::sort(m_strFilePathList.begin(), m_strFilePathList.end(), cf);

	ui->listWidget->clear();

	for (const QString& absolute_file_path : m_strFilePathList)
	{
		QFileInfo info(absolute_file_path);
		QString file_size = tr("%1").arg((float)(info.size() / 1000) / 1000);
		QDateTime dt = info.lastModified();
		QString modify_time = tr("%1").arg(dt.toString("yyyy-MM-dd hh:mm:ss"));

		QListWidgetItem* aItem;
		aItem = new QListWidgetItem();
		//aItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled);
		QString strText = info.fileName() + "\n" + file_size + "M " + modify_time + "\n" + absolute_file_path;
		//static QFont font = ui->listWidget->font();
		//font.setPointSize(TEXT_SIZE_DEFAULT);
		//aItem->setFont(font);
		aItem->setText(strText);
		ui->listWidget->insertItem(0, aItem);
	}
	ui->listWidget->update();
}
