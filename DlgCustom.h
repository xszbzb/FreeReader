#ifndef QDIALOGCUSTOM_H
#define QDIALOGCUSTOM_H

#include <QDialog>

class DlgCustom : public QDialog
{
	Q_OBJECT
public:
	DlgCustom(QWidget* parent = nullptr);

signals:
	void on_sig_close();
protected:

	void closeEvent(QCloseEvent* event);
	//bool event(QEvent* event) override;

protected Q_SLOTS:
	virtual void reject();
};

#endif // QDIALOGCUSTOM_H
