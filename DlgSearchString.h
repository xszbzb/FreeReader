#ifndef DIALOGSEARCHSTRING_H
#define DIALOGSEARCHSTRING_H

#include <QDialog>
#include "DlgCustom.h"
#include "ThreadCacheBook.h"

namespace Ui {
	class DlgSearchString;
}

class DlgSearchString : public DlgCustom
{
	Q_OBJECT

public:
	explicit DlgSearchString(QWidget* parent = nullptr);
	~DlgSearchString();
	void ListWidgetClear();

signals:
	void on_sig_query_book_string(int iChapter, int row, QString strRow);

public slots:
	void on_slot_query_book_string(int iChapter, int row, QString strRow);

private slots:
	void on_btnSearchText_clicked();

	void on_listWidgetSearchText_currentTextChanged(const QString& currentText);

	void on_spinBoxMaxCount_valueChanged(int arg1);

private:
	Ui::DlgSearchString* ui;

public:
	std::thread m_threadQueryBookString;
	bool m_threadQueryBookStringStop{ true };
};

#endif // DIALOGSEARCHSTRING_H
