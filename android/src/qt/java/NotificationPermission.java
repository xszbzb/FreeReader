package qt.java;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
//import androidx.core.app.NotificationManagerCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.content.ComponentName;
import android.util.Log;
import androidx.annotation.RequiresPermission;
import 	java.util.HashSet;
import 	java.lang.reflect.Method;
import 	android.Manifest;
import android.text.TextUtils;
import java.lang.reflect.InvocationTargetException;
import android.app.AppOpsManager;
import java.lang.reflect.Field;
import android.content.pm.ApplicationInfo;
import androidx.annotation.RequiresApi;
import android.app.Activity;

public class NotificationPermission {
    private static final String TAG = "NotificationPermission";

    public static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";

    private static final HashSet<ComponentName> mEnabledListeners = new HashSet<>();


    @RequiresPermission(allOf = {Manifest.permission.WRITE_SETTINGS, Manifest.permission.WRITE_SECURE_SETTINGS})
    public static void enableNotificationAccess(Context context) {
        if (isAccessibilityEnabled(context)) {
            Log.d(TAG, "enableNotificationAccess: the accessibility has been enabled");
            return;
        }

        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("android.provider.extra.APP_PACKAGE", context.getPackageName());
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {  //5.0
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("app_package", context.getPackageName());
            intent.putExtra("app_uid", context.getApplicationInfo().uid);
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {  //4.4
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + context.getPackageName()));
        } else if (Build.VERSION.SDK_INT >= 15) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", context.getPackageName(), null));
        }
        context.startActivity(intent);
    }

    /**
     * 判断当前应用是否开启NotificationListener监听权限
     *
     * @param context
     * @return
     */
    private static boolean isAccessibilityEnabled(Context context) {
        NotificationManagerCompat manager = NotificationManagerCompat.from(context);
        return manager.areNotificationsEnabled();
    }

    /**
     * 获取所有开启NotificationListener监听权限的组件
     *
     * @param context
     */
    private static void loadEnabledListener(Context context) {
        mEnabledListeners.clear();
        String notificationList = Settings.Secure.getString(context.getContentResolver(), ENABLED_NOTIFICATION_LISTENERS);
        if (!TextUtils.isEmpty(notificationList)) {
            final String[] names = notificationList.split(":");
            for (final String name : names) {
                ComponentName componentName = ComponentName.unflattenFromString(name);
                if (componentName != null) {
                    mEnabledListeners.add(componentName);
                }
            }
        }
    }


    /**
     * 去设置里手动打开NotificationListener监听权限
     *
     * @param context
     */
    public static void toOpenNotificationListenAccessManually(Context context) {
        try {
            Intent intent;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                intent = new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
            } else {
                intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            }
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
