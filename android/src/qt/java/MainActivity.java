package qt.java;

import org.qtproject.qt5.android.bindings.QtActivity;
import android.os.Bundle;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import qt.java.WhiteService;
import java.io.File;
import android.net.Uri;
import android.content.ActivityNotFoundException;
import android.os.Environment;
import android.util.Log;
import android.view.WindowManager;
import android.os.Build;

public class MainActivity extends QtActivity {
    private static Context mContext;
    public static native void CallNativeOnPause();
    public static native void CallNativeOnResume();
    //private static boolean bPause = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("MainActivity", "onCreate");
        super.onCreate(savedInstanceState);

        //获取context
        mContext = getApplicationContext();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);   //应用运行时，保持屏幕高亮，不锁屏

        Intent bindIntent = new Intent(MainActivity.this, WhiteService.class);
        bindService(bindIntent, mCon, Context.BIND_AUTO_CREATE);
        startService(new Intent(bindIntent));


    }

    protected void onResume()
    {
        Log.d("MainActivity", "onResume");
        CallNativeOnResume();
        super.onResume();
    }

    protected void onPause()
    {
        Log.d("MainActivity", "onPause");
        CallNativeOnPause();
        super.onPause();
    }

    private static ServiceConnection mCon = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            // TODO Auto-generated method stub

        }
    };

    //创建一个静态的方法，以便获取context对象
    public static Context getContext(){
        return mContext;
    }

    public void openAssignFolder(String path){
        Log.d("openAssignFolder: path:", path);
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            return;
        }
        path = Environment.getExternalStorageDirectory().getAbsolutePath()
        + "/FreeReader/BooksInfo/";
        Log.d("openAssignFolder: getAbsolutePath:", path);

        File file = new File(path);
        if(null==file || !file.exists()){
            return;
        }

        //调用系统文件管理器打开指定路径目录
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(Uri.fromFile(file), "file/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivity(intent);

        //Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        //intent.addCategory(Intent.CATEGORY_DEFAULT);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //intent.setDataAndType(Uri.fromFile(file), "*/*");
        //try {
        //        startActivity(intent);
        //        //startActivity(Intent.createChooser(intent,"选择浏览工具"));
        //} catch (ActivityNotFoundException e) {
        //        e.printStackTrace();
        //}
    }
}
