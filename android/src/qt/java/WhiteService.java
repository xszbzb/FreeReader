package qt.java;
 
import android.content.Context;
import org.qtproject.qt5.android.bindings.QtService;
import org.qtproject.qt5.android.bindings.QtActivity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.Nullable;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.content.ComponentName;
import android.app.Activity;
import android.graphics.BitmapFactory;
import org.qtproject.FreeReader.R;
import android.graphics.Color;

public class WhiteService extends QtService {
    private static final String TAG = WhiteService.class.getSimpleName();
    private static final int NOTIFICATION_FLAG =0X18;

    public static void startMyService(Context ctx) {
        Log.d(TAG, "startService start");
        ctx.startService(new Intent(ctx, WhiteService.class));
        Log.d(TAG, "startService end");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        //throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "Service on create");//服务被创建
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand start");

        String CHANNEL_ONE_ID = "保持后台运行通知";
        String CHANNEL_ONE_NAME = "保持后台运行";
        NotificationChannel notificationChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
           notificationChannel = new NotificationChannel(CHANNEL_ONE_ID,
                   CHANNEL_ONE_NAME, NotificationManager.IMPORTANCE_HIGH);
           notificationChannel.enableLights(true);
           notificationChannel.setLightColor(Color.RED);
           notificationChannel.setShowBadge(true);
           notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
           NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
           manager.createNotificationChannel(notificationChannel);
        }

        // 在Android进行通知处理，首先需要重系统哪里获得通知管理器NotificationManager，它是一个系统Service。
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // 设置点击通知跳转的Intent
        Intent nfIntent = new Intent(this, MainActivity.class);
        // 设置 延迟Intent
        // 最后一个参数可以为PendingIntent.FLAG_CANCEL_CURRENT 或者 PendingIntent.FLAG_UPDATE_CURRENT
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, nfIntent, 0);

        //构建一个Notification构造器
        Notification.Builder builder = new Notification.Builder(this.getApplicationContext());

        //int resId = this.getResources().getIdentifier("icon", "drawable" ,
        //    this.getApplicationContext().getPackageName());
        builder.setContentIntent(pendingIntent)   // 设置点击跳转界面
                .setChannelId(CHANNEL_ONE_ID)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                        R.drawable.icon)) // 设置下拉列表中的图标(大图标)
                .setTicker("飞阅")// statusBar上的提示
                .setContentTitle("飞天阅读器") // 设置下拉列表里的标题
                .setSmallIcon(R.drawable.icon) // 设置状态栏内的小图标24X24
                .setContentText("运行中") // 设置详细内容
                .setContentIntent(pendingIntent) // 设置点击跳转的界面
                .setWhen(System.currentTimeMillis()) // 设置该通知发生的时间
                .setDefaults(Notification.DEFAULT_VIBRATE) //默认震动方式
                .setPriority(Notification.PRIORITY_HIGH);   //优先级高

        Notification notification = builder.build(); // 获取构建好的Notification

        notification.defaults = Notification.DEFAULT_SOUND; //设置为默认的声音
        notification.flags |= Notification.FLAG_AUTO_CANCEL; // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
        notification.flags |= Notification.FLAG_ONGOING_EVENT; //将此通知放到通知栏的"Ongoing"即"正在运行"组中
        notification.flags |= Notification.FLAG_NO_CLEAR; //表明在点击了通知栏中的"清除通知"后，此通知不清除，常与FLAG_ONGOING_EVENT一起使用

        manager.notify(NOTIFICATION_FLAG, notification);

        // 启动前台服务
        // 参数一：唯一的通知标识；参数二：通知消息。
        startForeground(NOTIFICATION_FLAG, notification);// 开始前台服务

        Log.d(TAG, "onStartCommand end");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // 停止前台服务--参数：表示是否移除之前的通知
        stopForeground(true);
        Log.d(TAG, "onDestroy");
    }

}

