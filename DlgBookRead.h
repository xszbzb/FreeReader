﻿#ifndef BOOKREADDLG_H
#define BOOKREADDLG_H

#include <QDialog>
#include <QTextToSpeech>
#include "common.h"
#include "DlgContents.h"
#include "DlgSearchString.h"
#include "ThreadCacheBook.h"
#include "DlgColorDrag.h"

namespace Ui {
	class DlgBookRead;
}

class DlgBookRead : public DlgCustom
{
	Q_OBJECT

public:
	void initTTS();
	void play();
	void stop();
	void replay();

	virtual void timerEvent(QTimerEvent* event);

	int ParseFile();
	void UpdateChapter();

public:
	explicit DlgBookRead(QWidget* parent = nullptr);
	~DlgBookRead();
	auto& getVChapter() { return m_chapterList; };
	void MoveCursorToLine();
	bool FindChar(QString& str);
	//章头开始查找下一个匹配的章节头,返回当前章节头的尾部索引
	int FindChapterTail(const QString& m_qstrBookFileBuf, const int index);

signals:
	void on_sig_refresh_finish(bool b);
public slots:
	void on_slots_cache_finish();
	void on_slots_refresh_finish(bool b);
	void on_slots_colorSelected(const QColor& color);

	void engineSelected(int index);

private slots:

	void localeChanged(const QLocale& locale);

	void reject();

	void on_btnSetBackgroundColor_clicked();

	//上一章
	void on_btnPrev_clicked();
	//下一章
	void on_btnNext_clicked();

	void on_btnPrevPage_clicked();

	void on_btnNextPage_clicked();

	void on_btnContents_clicked();

	//语音
public slots:
	void on_btnPlay_clicked();
private slots:
	void PlayNextBlock();
	void setRate(int);
	void setPitch(int);
	void setVolume(int volume);
	void stateChanged(QTextToSpeech::State state);
	void languageSelected(int language);
	void voiceSelected(int index);

	void on_cbTextSize_currentTextChanged(QString str);
	void on_textEdit_clicked();
	void on_fontComboBox_currentTextChanged(QString);

	void on_btnMenu_clicked();

	void on_comboBoxTimer_currentIndexChanged(const QString& arg1);

	void on_btnRefreshCurChapter_clicked();

	void on_btnSearchString_clicked();

	void on_btnClearCache_clicked();

public slots:
	void on_btnCacheWebBook_clicked();

private:
	int m_nTimerID = 0;

	QVector<QVoice> m_voices;
	ThreadCacheBook m_threadCacheBook;
	DlgColorDrag m_dlgColor;
	bool m_bNextChapter = false;

public:
	Ui::DlgBookRead* ui;
	vector<BookChapterList> m_chapterList;
	DlgContents m_dlgContents;
	DlgSearchString m_dlgSearchString;
	QTextToSpeech* m_speech = NULL;

	enum E_PlayState {
		Stop,
		Play,
		PlayCur
	};

	//0:不播放状态; 1:播放下一个状态; 3:replay,重新播放当前串
	E_PlayState m_playState = Stop;
	QTextToSpeech::State m_stateTTS = QTextToSpeech::Ready;
	QString msg_cache_finish;

};

#endif // BOOKREADDLG_H
