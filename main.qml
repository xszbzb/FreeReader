import QtQuick 2.2
import QtQuick.Controls 1.1
import QtWebView 1.1
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.2


Item {
    visible: true
//    title: qsTr("Hello World")
    Rectangle {
        Text {
            text: webView.url
        }
    }

    Connections {
        target: widget
        onSigGo: {
            console.log("url: " + url);
            webView.url = url
            widget.setText(webView.url)
        }

        onSigReflash: {
            webView.reload()
            widget.setText(webView.url)
            console.log("url: " + webView.url)
        }

        onSigForward: {
            webView.goForward()
            widget.setText(webView.url)
            console.log("url: " + webView.url)
        }

        onSigBack: {
            webView.goBack()
            widget.setText(webView.url)
            console.log("url: " + webView.url)
        }

    }

    WebView {
        id: webView
        anchors.fill: parent
        onLoadingChanged: {
            widget.setText(webView.url)
            console.log("url changed: " + webView.url)
            if (loadRequest.errorString)
                console.error(loadRequest.errorString)
        }
    }

}
