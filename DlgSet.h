#ifndef DIALOGSET_H
#define DIALOGSET_H

#include <QDialog>
#include "common.h"

namespace Ui {
	class DlgSet;
}

class DlgSet : public DlgCustom
{
	Q_OBJECT

public:
	explicit DlgSet(QWidget* parent = nullptr);
	~DlgSet();

private slots:
	void on_btnClearAllSet_clicked();

	void on_btnClearWebBookCache_clicked();

	void reject();

	void FilterStringToMap(const QString& strRegularFilter);

	void on_btnOpenSetDir_clicked();

	void on_spinBoxGetWebRepeatTimes_valueChanged(int arg1);

private:
	Ui::DlgSet* ui;
};

#endif // DIALOGSET_H
