﻿#include "DlgSet.h"
#include "ui_DlgSet.h"
#include "common.h"
#include "MainWindow.h"
#include "QTextEditSlide.h"
#include <QDesktopServices>

DlgSet::DlgSet(QWidget* parent) :
	DlgCustom(parent),
	ui(new Ui::DlgSet)
{
	ui->setupUi(this);
	setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);
	QString strTextFilter = g_set->value("global/regular_filter",
		"[　\\r\\n\\t]{3,}#\\n　\n"
		".*手机版网址.*\n"
		"&.*;\n"
		".*月票.*\n"
		"\\[.*\\]\n"
		"（.*）\n"
		".*推荐本书.*\n"
		"\\(.*\\)\n"
		"txt下载地址：\n"
		"手机阅读：\n"
		"PS.*\n"
		"ps.*\n"
		".*最快更新.*\n"
		"【.*】\n"
		"推荐.*：\n"
	).toString();
	ui->textEditRegularFilter->setText(strTextFilter);
	int value = g_set->value("global/get_web_repeat_times", GET_WEB_REPEAT_TIMES).toInt();
	ui->spinBoxGetWebRepeatTimes->setValue(value);
	FilterStringToMap(strTextFilter);
}

DlgSet::~DlgSet()
{
	delete ui;
}

void DlgSet::on_btnClearAllSet_clicked()
{
	QString del_file = QString(BOOK_LIST_SAVE_DIR);
	QDir dir;
	dir.setPath(del_file);
	dir.removeRecursively();
	exit(0);
}

void DlgSet::on_btnClearWebBookCache_clicked()
{
	//QDir dir;
	//dir.setPath(del_file);
	//dir.removeRecursively();

	//DelSuffixFile(del_file.toLocal8Bit().data(), "chapter");
	//DelSuffixFile("D:\\FreeReader\\BooksInfo\\cache", "chapter");

	static std::thread s_thread;
	static auto stop = true;

	auto run = [&]()
		{
			stop = false;
			QString del_file = QString(BOOK_LIST_SAVE_DIR"cache");
			string strPath = del_file.toLocal8Bit().data();
			QString strBtnText = ui->btnClearWebBookCache->text();
			ui->btnClearWebBookCache->setText(strBtnText + BUTTON_IN_PROGRESS);
			DelSuffixFile(strPath, "chapter", stop, 5);
			ui->btnClearWebBookCache->setText(strBtnText);
		};

	if (s_thread.joinable()) {
		stop = true;
		s_thread.join();
	}
	else
	{
		s_thread = std::thread(run);
		s_thread.detach();
	}
}

void DlgSet::reject()
{
	QString str = ui->textEditRegularFilter->toPlainText();
	g_set->setValue("global/regular_filter", str);
	FilterStringToMap(str);
	g_set->setValue("global/get_web_repeat_times", g_get_web_repeat_times);
	DlgCustom::reject();
}

void DlgSet::FilterStringToMap(const QString& strRegularFilter)
{
	g_mapRegularFilter.clear();
	QStringList list = strRegularFilter.split("\n", QString::SkipEmptyParts); //按行分割字符串
	for (auto& str : list)
	{
		QStringList listReg = str.split("#", QString::SkipEmptyParts);
		if (listReg.size() == 0 || listReg[0].size() == 0)
			continue;
		else if (listReg.size() == 1)
		{
			g_mapRegularFilter[listReg[0].toStdWString()];
		}
		else if (listReg.size() == 2)
		{
			listReg[1].replace("\\r", "\r");
			listReg[1].replace("\\n", "\n");
			listReg[1].replace("\\t", "\t");
			listReg[1].replace("\\f", "\f");
			listReg[1].replace("\\v", "\v");
			listReg[1].replace("\\0", "\0");
			g_mapRegularFilter[listReg[0].toStdWString()] = listReg[1].toStdWString();
		}
	}
}

#define CHECK_EXCEPTION() \
    if(env->ExceptionCheck())\
    {\
        qDebug() << "exception occured";\
        env->ExceptionClear();\
    }

void DlgSet::on_btnOpenSetDir_clicked()
{
	auto url = QUrl::fromLocalFile(BOOK_LIST_SAVE_DIR);
	if (!QDesktopServices::openUrl(url))
	{
		QString dlgTitle = ui->btnOpenSetDir->text();
		QString strInfo = QString::asprintf(
			"配置文件目录: %s", BOOK_LIST_SAVE_DIR);
		QMessageBox::about(this, dlgTitle, strInfo);
	}
#ifdef __ANDROID__
	//QAndroidJniEnvironment env;
	//QAndroidJniObject activity = QtAndroid::androidActivity();
	//QAndroidJniObject className =
	//	activity.callObjectMethod<jstring>("getLocalClassName");
	//CHECK_EXCEPTION()
	//	QString name = className.toString();
	//QAndroidJniObject strPath = QAndroidJniObject::fromString(
	//	BOOK_LIST_SAVE_DIR);
	//activity.callMethod<void>("openAssignFolder", "(Ljava/lang/String;)V", strPath.object<jstring>());
	//QAndroidJniObject myNewJavaString("java/lang/String", "(Ljava/lang/String;)V", myJStringArg);

	//QAndroidJniObject string1 = QAndroidJniObject::fromString(BOOK_LIST_SAVE_DIR);
	//QAndroidJniObject::callStaticMethod<void>(
	//	"qt/java/MainActivity",
	//	"openAssignFolder",
	//	"(Ljava/lang/String;)V",
	//	string1.object<jstring>());
#endif
	}

void DlgSet::on_spinBoxGetWebRepeatTimes_valueChanged(int arg1)
{
	g_get_web_repeat_times = arg1;
}
