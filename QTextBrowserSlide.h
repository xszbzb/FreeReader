#ifndef QTEXTBROWSERSLIDE_H
#define QTEXTBROWSERSLIDE_H


#include <QObject>
#include <QTextBrowser>
#include <QTimer>

class QTextBrowserSlide : public QTextBrowser
{
	Q_OBJECT
public:
	QTextBrowserSlide(QWidget* parent = Q_NULLPTR);
	virtual void timerEvent(QTimerEvent* event);
private:
	int m_nTimerID = 0;
	int m_slideSpeed = 0;//执行次数
	qint64 m_MillisecondTimeDifference = 0;

protected:

	void mousePressEvent(QMouseEvent* event);        //单击
	void mouseReleaseEvent(QMouseEvent* event);      //释放
	void mouseMoveEvent(QMouseEvent* event);         //移动
	void mouseDoubleClickEvent(QMouseEvent* e);

signals:
	void on_sig_clicked();

public:
	QPoint m_point;
private:
	int m_origin = 0;
};

#endif// QTEXTBROWSERSLIDE_H
