# FreeReader

#### 介绍
QT做的小说阅读器

#### 软件架构
软件架构说明


#### 开发需要的软件

1.  Qt5.12.5
2.  VS2019

#### 开发注意事项
1. VS2019工程属性->C/C++->所有选项->附加选项-> 添加 /utf-8
2. 控件不要同名并使用自动生成的槽函数，会发生错乱，重复执行等问题
3. QT中为了线程安全，不能在子线程中创建UI，需要使用信号和槽的传递方式
4. 工具-自定义-命令-菜单栏-文件-添加命令-文件-高级保存选项


#### 使用说明

手机和windows可以安装使用

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
