QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += multimedia multimediawidgets
QT += texttospeech
android:QT += androidextras
QT += network

RC_ICONS = freereader.ico

CONFIG += c++17
QMAKE_CXXFLAGS += -std=c++17
CONFIG -= qml_debug
android: include(D:/lib/openssl/android_openssl-master/openssl.pri)
# android: QMAKE_CXXFLAGS += -Xlint:deprecation


QT += qml quick webview quickwidgets
RESOURCES += qml.qrc
macos:QMAKE_INFO_PLIST = macos/Info.plist
ios:QMAKE_INFO_PLIST = ios/Info.plist

# win32:INCLUDEPATH += $(QTDIR)\include\QtQml
# INCLUDEPATH += $(QTDIR)\include\ActiveQt
# win32:LIBS += -L$$PWD/../../../Qt/Qt5.12.5/5.12.5/msvc2017_64/lib/
# linux:LIBS += -L$$PWD/../../../Qt/Qt5.12.5/5.12.5/android_arm64_v8a/lib/
# LIBS += -lQt5AxContainer -lQt5AxBase
#android: LIBS += -LD:/lib/openssl/openssl-armeabi-v7a/lib -lssl -lcrypto

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    DlgAddBook.cpp \
    DlgBookCity.cpp \
    DlgBookRead.cpp \
    DlgColorDrag.cpp \
    DlgContents.cpp \
    DlgCustom.cpp \
    DlgSearchString.cpp \
    DlgSet.cpp \
    DlgWebWidget.cpp \
    EX_Slider.cpp \
    MainWindow.cpp \
    QListWidgetSlide.cpp \
    QTextBrowserSlide.cpp \
    QTextEditSlide.cpp \
    QWidgetDrag.cpp \
    ThreadCacheBook.cpp \
    common.cpp \
    log.c \
    main.cpp

HEADERS += \
    DlgAddBook.h \
    DlgBookCity.h \
    DlgBookRead.h \
    DlgColorDrag.h \
    DlgContents.h \
    DlgCustom.h \
    DlgSearchString.h \
    DlgSet.h \
    DlgWebWidget.h \
    EX_Slider.h \
    MainWindow.h \
    QComboBoxEx.h \
    QListWidgetSlide.h \
    QTextBrowserSlide.h \
    QTextEditSlide.h \
    QWidgetDrag.h \
    ThreadCacheBook.h \
    common.h \
    log.h \
    resource.h \
    version.h

FORMS += \
    DlgAddBook.ui \
    DlgBookCity.ui \
    DlgBookRead.ui \
    DlgContents.ui \
    DlgSearchString.ui \
    DlgSet.ui \
    DlgWebWidget.ui \
    MainWindow.ui

TRANSLATIONS += \
    FreeReader_zh_CN.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    images.qrc

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/drawable/icon.png \
    android/res/values/libs.xml \
    android/src/qt/java/FcfrtAppBhUtils.java \
    android/src/qt/java/MainActivity.java \
    android/src/qt/java/NotificationPermission.java \
    android/src/qt/java/WhiteService.java


contains(ANDROID_TARGET_ARCH,arm64-v8a) {
    message("arm64-v8a")
    ANDROID_EXTRA_LIBS = \
    $$PWD/../../../lib/openssl/android_openssl-master/latest/arm64/libcrypto_1_1.so \
    $$PWD/../../../lib/openssl/android_openssl-master/latest/arm64/libssl_1_1.so

    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    message("armeabi-v7a")
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android

    ANDROID_EXTRA_LIBS = \
        D:/QtProjects/FreeReader/FreeReader/../../../lib/openssl/android_openssl-master/latest/arm/libssl_1_1.so \
        D:/QtProjects/FreeReader/FreeReader/../../../lib/openssl/android_openssl-master/latest/arm/libcrypto_1_1.so
}
