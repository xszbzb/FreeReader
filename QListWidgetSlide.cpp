﻿#include "QListWidgetSlide.h"
#include <QMouseEvent>
#include <QScrollBar>
#include <QDateTime>
#include "common.h"


QListWidgetSlide::QListWidgetSlide(QWidget* parent)
	: QListWidget(parent)
{
	setVerticalScrollMode(ScrollPerPixel);
	//setSpacing(4);
	setStyleSheet(
		//"QListWidget{border:1px solid gray; color:black; }"
		//"QListWidget::Item{padding-top:-2px; padding-bottom:-1px;}"
		//"QListWidget::Item:hover{background:skyblue;padding-top:0px; padding-bottom:0px; }"
		"QListWidget::Item{background-color: qlineargradient(spread:pad, x1:0, y1:1, x2:0, y2:0, stop:0 rgba(100, 170, 100, 255), stop:1 rgba(220, 220, 220, 255));}"
		//"QListWidget::item:selected:!active{active{border-width:0px;background:lightgreen; }"
		"QListWidget::item:selected{ background:qlineargradient(spread:pad, x1:0, y1:1, x2:0, y2:0, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(255, 255, 255, 255)); color:red; }"
	);
}


//单击
void QListWidgetSlide::mousePressEvent(QMouseEvent* event)
{
	//qDebug() << tr("(%1,%2)").arg(event->x()).arg(event->y());
	if (event->button() == Qt::LeftButton)
	{
		//记录鼠标的世界坐标.
		m_point = event->globalPos();
		m_origin = verticalScrollBar()->value();
	}
	if (m_nTimerID != 0)
	{
		killTimer(m_nTimerID);
		m_nTimerID = 0;
	}
	m_MillisecondTimeDifference = QDateTime::currentDateTime().toMSecsSinceEpoch();
	//QListWidget::mousePressEvent(event);
	return;
}

//释放
void QListWidgetSlide::mouseReleaseEvent(QMouseEvent* event)
{
	QPoint point = event->globalPos();
	point = m_point - point;
	m_MillisecondTimeDifference = (QDateTime::currentDateTime().toMSecsSinceEpoch() - m_MillisecondTimeDifference);
	if (point.manhattanLength() < fontMetrics().lineSpacing())
	{
		if (m_MillisecondTimeDifference < MAX_TIME_NOT_TO_CLICK)
		{
			QListWidget::mousePressEvent(event);
			QListWidget::mouseReleaseEvent(event);
			QListWidgetItem* item = currentItem();
			bool b = isItemSelected(item);
			setItemSelected(item, !b);
		}
	}
	else
	{
		if (m_MillisecondTimeDifference < MAX_TIME_NOT_TO_CLICK && m_MillisecondTimeDifference > 0)
		{
			m_slideSpeed = m_slideSpeed * SLIDE_ONE_TIME / m_MillisecondTimeDifference;
			m_nTimerID = startTimer(SLIDE_ONE_TIME);
		}
	}
	return;
}

//移动
void QListWidgetSlide::mouseMoveEvent(QMouseEvent* event)
{
	if (event->buttons() & Qt::LeftButton)
	{
		//移动中的鼠标位置相对于初始位置的相对位置.
		QPoint relativePos = event->globalPos() - m_point;
		//然后移动窗体即可.
		int len = relativePos.y();
		m_slideSpeed = len;
		verticalScrollBar()->setValue(m_origin - m_slideSpeed);
	}
	else {
		QListWidget::mouseMoveEvent(event);
	}
	return;
}

void QListWidgetSlide::timerEvent(QTimerEvent* event)
{
	if (event->timerId() == m_nTimerID) {
		if (m_slideSpeed != 0)
		{
			m_slideSpeed > 0 ? m_slideSpeed-- : m_slideSpeed++;
			verticalScrollBar()->setValue(verticalScrollBar()->value() - m_slideSpeed);
		}
		else
		{
			if (m_nTimerID != 0)
			{
				killTimer(m_nTimerID);
				m_nTimerID = 0;
			}
		}
	}
}
