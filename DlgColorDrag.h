#ifndef QCOLORDIALOGDRAG_H
#define QCOLORDIALOGDRAG_H

//#include "qwidgetdrag.h"
#include <QColorDialog>
#include <QMouseEvent>

class DlgColorDrag : public QColorDialog
{

	Q_OBJECT
public:
	DlgColorDrag(QWidget* parent = nullptr);

protected:
	virtual void mousePressEvent(QMouseEvent* event);
	virtual void mouseMoveEvent(QMouseEvent* event);
	virtual void mouseReleaseEvent(QMouseEvent* event);

private:
	bool m_move;
	QPoint m_startPoint;
	QPoint m_windowPoint;
};

#endif // QCOLORDIALOGDRAG_H
