#ifndef QTEXTEDITSLIDE_H
#define QTEXTEDITSLIDE_H

#include <QObject>
#include <QTextEdit>
#include <QTimer>

class QTextEditSlide : public QTextEdit
{
	Q_OBJECT
public:
	QTextEditSlide(QWidget* parent = Q_NULLPTR);
	virtual void timerEvent(QTimerEvent* event);
private:
	int m_nTimerID = 0;
	int m_slideSpeed = 0;//执行次数
	qint64 m_MillisecondTimeDifference = 0;

protected:

	virtual void mousePressEvent(QMouseEvent* event);        //单击
	virtual void mouseReleaseEvent(QMouseEvent* event);      //释放
	virtual void mouseMoveEvent(QMouseEvent* event);         //移动
	virtual void mouseDoubleClickEvent(QMouseEvent* e);
	virtual void on_clicked(QMouseEvent* event);

public:
	QPoint m_point;

signals:
	void on_sig_clicked();

private:
	int m_origin = 0;
	QMouseEvent* m_oldPressEvent;
};

class QTextEditSlideSimple : public QTextEditSlide
{
	Q_OBJECT
public:
	QTextEditSlideSimple(QWidget* parent = Q_NULLPTR) :QTextEditSlide(parent) {};

protected:
	virtual void mouseDoubleClickEvent(QMouseEvent* e);
	virtual void on_clicked(QMouseEvent* event);
};

#endif
