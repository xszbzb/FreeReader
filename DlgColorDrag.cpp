﻿#include "DlgColorDrag.h"


DlgColorDrag::DlgColorDrag(QWidget* parent) : QColorDialog(parent)
{
	//给是否移动的标志初始化为false.
	m_move = false;
}
void DlgColorDrag::mousePressEvent(QMouseEvent* event)
{
	//当鼠标左键点击时.
	if (event->button() == Qt::LeftButton)
	{
		m_move = true;
		//记录鼠标的世界坐标.
		m_startPoint = event->globalPos();
		//记录窗体的世界坐标.
		m_windowPoint = this->frameGeometry().topLeft();
	}
	QWidget::mousePressEvent(event);
}
void DlgColorDrag::mouseMoveEvent(QMouseEvent* event)
{
	if (event->buttons() & Qt::LeftButton)
	{
		//移动中的鼠标位置相对于初始位置的相对位置.
		QPoint relativePos = event->globalPos() - m_startPoint;
		//然后移动窗体即可.
		this->move(m_windowPoint + relativePos);
	}
	QWidget::mouseMoveEvent(event);
}
void DlgColorDrag::mouseReleaseEvent(QMouseEvent* event)
{
	if (event->button() == Qt::LeftButton)
	{
		//改变移动状态.
		m_move = false;
	}
	QWidget::mouseReleaseEvent(event);
}
