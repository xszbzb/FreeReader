﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include "DlgBookRead.h"
#include "common.h"
#include "DlgAddBook.h"
#include "DlgBookCity.h"
#include "DlgSet.h"
#include "DlgWebWidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

		void ReadBookListInfo();

public:
	MainWindow(QWidget* parent = nullptr);
	~MainWindow();
	void updateListWidgetBook();
	void setGlobalFont();
	void AddBookInfoToWeiget(sBookInfo book, int indexItem = 0);
	void DelBook(QString strPath);
	void DelBook(size_t row);
	void CallNativeOnPause();
	void CallNativeOnResume();

private slots:
	void on_listWidgetBook_itemClicked(QListWidgetItem* item);

	void on_btnAddBook_clicked();

	void on_btnDeleteBook_clicked();


	void on_btnClearBook_clicked();

	void on_btnAddBookAuto_clicked();

	void on_btnAbout_clicked();

	void on_btnBookCity_clicked();

	void on_btnSet_clicked();

	void on_btnProcessExit_clicked();

	void on_btnUpdateBook_clicked();

private:
	bool m_bDeleteBook = 0;

	Ui::MainWindow* ui;
	DlgAddBook m_dlgAutoAddBook;
	DlgBookCity m_dlgBookCity;
	DlgSet m_dlgSet;
	DlgBookRead m_dlgBookRead;

#if WEB_WIDGET_ENABLE
	DlgWebWidget m_webWidget;
#endif
};
#endif // MAINWINDOW_H
