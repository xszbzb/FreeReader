#ifndef QCOMBOBOXEX_H
#define QCOMBOBOXEX_H

#include <QComboBox>
#include <QKeyEvent>

class QComboBoxEx : public QComboBox
{
	Q_OBJECT
public:
    QComboBoxEx(QWidget* parent = nullptr):QComboBox(parent){}

signals:
	void on_sig_enter();

protected:
	bool event(QEvent* event)
	{
		switch (event->type())
		{
		case QEvent::KeyPress:
			if (((QKeyEvent*)event)->key() == Qt::Key_Return) {
				emit on_sig_enter();
				return true;
			}
			break;
		case QEvent::KeyRelease:
			if (((QKeyEvent*)event)->key() == Qt::Key_Return) {
				return true;
			}
			break;
		}
		return QComboBox::event(event);
	}
};

#endif // QCOMBOBOXEX_H
