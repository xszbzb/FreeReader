#ifndef EX_SLIDER_H
#define EX_SLIDER_H

#include <QObject>
#include <QWidget>
#include <QSlider>

class EX_Slider : public QSlider
{
public:
    EX_Slider(QWidget *parent = nullptr);
    ~EX_Slider();

    void mousePressEvent(QMouseEvent *ev);
};

#endif // EX_SLIDER_H
