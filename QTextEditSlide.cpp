﻿#include "QTextEditSlide.h"
#include <QMouseEvent>
#include <QScrollBar>
#include <QDateTime>
#include "common.h"

static QPoint sPosMousePressEvent = { 0, 0 };

QTextEditSlide::QTextEditSlide(QWidget* parent)
	: QTextEdit(parent)
{
	//setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}


//单击
void QTextEditSlide::mousePressEvent(QMouseEvent* event)
{
	sPosMousePressEvent = event->pos();
	//qDebug() << tr("(%1,%2)").arg(event->x()).arg(event->y());
	if (event->button() == Qt::LeftButton)
	{
		//记录鼠标的世界坐标.
		m_point = event->globalPos();
		m_origin = verticalScrollBar()->value();
		//记录窗体的世界坐标.
		//m_pos = this->frameGeometry().topLeft();
	}
	if (m_nTimerID != 0)
	{
		killTimer(m_nTimerID);
		m_nTimerID = 0;
	}
	m_MillisecondTimeDifference = QDateTime::currentDateTime().toMSecsSinceEpoch();
	//m_bDrag = false;
	//QTextEdit::mousePressEvent(event);
	return;
}

//释放
void QTextEditSlide::mouseReleaseEvent(QMouseEvent* event)
{
	//qDebug() << tr("(%1,%2)").arg(event->x()).arg(event->y());
	//if (!m_bDrag)
	QPoint point = event->globalPos();
	//QPoint pt = point;
	point = m_point - point;
	m_MillisecondTimeDifference = (QDateTime::currentDateTime().toMSecsSinceEpoch() - m_MillisecondTimeDifference);
	if (point.manhattanLength() < fontMetrics().lineSpacing())
	{
		if (m_MillisecondTimeDifference < MAX_TIME_NOT_TO_CLICK)
		{
			on_clicked(event);
		}
	}
	else
	{
		if (m_MillisecondTimeDifference < MAX_TIME_NOT_TO_CLICK && m_MillisecondTimeDifference > 0)
		{
			m_slideSpeed = m_slideSpeed * SLIDE_ONE_TIME / m_MillisecondTimeDifference;
			m_nTimerID = startTimer(SLIDE_ONE_TIME);
		}
	}
	return;
}

//移动
void QTextEditSlide::mouseMoveEvent(QMouseEvent* event)
{
	//QString strText(tr("(%1,%2)").arg(event->x()).arg(event->y()));

	if (event->buttons() & Qt::LeftButton)
	{
		//移动中的鼠标位置相对于初始位置的相对位置.
		QPoint relativePos = event->globalPos() - m_point;
		//然后移动窗体即可.
		//this->move(m_pos + relativePos);
		//qDebug() << "itemAt(10,0) :" << itemAt(10, 0); //输出(10,0)点的图形项
		//qDebug() << "itemAt(30,0) :" << itemAt(30, 0);
		//qDebug() << "#################################"; //分割线
		//setToolTip(QString::asprintf("%d, %d", verticalScrollBar()->value(), len));
		//verticalOffset();
		int len = relativePos.y();
		//int lineSpacing = fontMetrics().lineSpacing();//leading()+height()
		m_slideSpeed = len;
		verticalScrollBar()->setValue(m_origin - m_slideSpeed);
		//m_bDrag = true;
		//verticalScrollBar()->setValue(m_origin - (len > 0 ? 1 : -1));
		//itemAt(geometry().topLeft());
		//pos();
	}
	else {
		QTextEdit::mouseMoveEvent(event);
	}
	return;
}

void QTextEditSlide::timerEvent(QTimerEvent* event)
{
	if (event->timerId() == m_nTimerID) {
		if (m_slideSpeed != 0)
		{
			m_slideSpeed > 0 ? m_slideSpeed-- : m_slideSpeed++;
			verticalScrollBar()->setValue(verticalScrollBar()->value() - m_slideSpeed);
		}
		else
		{
			if (m_nTimerID != 0)
			{
				killTimer(m_nTimerID);
				m_nTimerID = 0;
			}
		}
	}
}

void QTextEditSlide::mouseDoubleClickEvent(QMouseEvent*)
{
	//Qt::MouseButton mb = e->button();
	//int test = 0;
	//Q_D(QTextEditSlide);
	//d->sendControlEvent(e);
}

void QTextEditSlide::on_clicked(QMouseEvent*)
{
	emit on_sig_clicked();
}

void QTextEditSlideSimple::mouseDoubleClickEvent(QMouseEvent* e) {
	QTextEdit::mouseDoubleClickEvent(e);
};

void QTextEditSlideSimple::on_clicked(QMouseEvent* event)
{
	QMouseEvent eventPress(QEvent::MouseButtonPress, sPosMousePressEvent, Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
	QTextEdit::mousePressEvent(&eventPress);
	QMouseEvent eventMove(QEvent::MouseMove, event->pos(), Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
	QTextEdit::mouseMoveEvent(&eventMove);
	QTextEdit::mouseReleaseEvent(event);
}
