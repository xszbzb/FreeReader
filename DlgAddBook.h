#ifndef DIALOGADDBOOK_H
#define DIALOGADDBOOK_H

#include <QDialog>
#include <vector>
#include "common.h"

namespace Ui {
	class DlgAddBook;
}

class DlgAddBook : public DlgCustom
{
	Q_OBJECT

public:
	explicit DlgAddBook(QWidget* parent = nullptr);
	~DlgAddBook();
	QStringList selectedFiles();
	int FindFile(const QString& strFilePath, int layer = 3);


private slots:
	void on_btnFindBook_clicked();

	void on_checkBox_stateChanged(int arg1);

	void on_comboBoxSortOrder_currentTextChanged(const QString& arg1);

private:
	Ui::DlgAddBook* ui;
	std::vector<QString> m_strFilePathList;
};

#endif // DIALOGADDBOOK_H
