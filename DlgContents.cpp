﻿#include "DlgContents.h"
#include "ui_DlgContents.h"
#include <QColorDialog>
#include <QLoggingCategory>
#include <QTextLayout>
#include <QTextBlock>
#include <QScrollBar>
#include <QMessageBox>
#include "DlgBookRead.h"
#include "DlgBookRead.h"
#include "ThreadCacheBook.h"
#include "MainWindow.h"

static QString UPDATE_LIST_STRING;
//#define UPDATE_LIST_STRING tr("更新目录")
#define UPDATING_LIST_STRING (UPDATE_LIST_STRING+BUTTON_IN_PROGRESS)

DlgContents::DlgContents(QWidget* parent) :
	DlgCustom(parent),
	ui(new Ui::DlgContents)
{
	ui->setupUi(this);
	setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);
	//setWindowOpacity(0.8);//设置透明度
	connect(this, &DlgContents::on_sig_update_contents, this, &DlgContents::on_slots_update_contents);
	UPDATE_LIST_STRING = ui->btnUpdateList->text();
}

DlgContents::~DlgContents()
{
	delete ui;
}

void DlgContents::ListWidgetClear()
{
	ui->listWidgetContents->clear();
}

void DlgContents::UpdateContents()
{
	ui->btnUpdateList->setText(UPDATING_LIST_STRING);
	ui->cbSort->setCheckState(m_listBookInfo[0].sort);
	int counter = ui->listWidgetContents->count();
	if (counter == 0)
	{
		QListWidgetItem* aItem; //每一行是一个QListWidgetItem
		for (auto& strChapterHead : ((DlgBookRead*)parent())->m_chapterList)
		{
			aItem = new QListWidgetItem(); //新建一个项
			aItem->setText(strChapterHead.head); //设置文字标签
			ui->listWidgetContents->addItem(aItem); //增加一个项
		}
	}
	QListWidgetItem* item = ui->listWidgetContents->item(m_listBookInfo[0].chapter);
	ui->listWidgetContents->scrollToItem(ui->listWidgetContents->item(m_listBookInfo[0].chapter),
		QAbstractItemView::ScrollHint::PositionAtCenter);
	ui->listWidgetContents->setItemSelected(item, true);
	ui->btnUpdateList->setText(UPDATE_LIST_STRING);
}

void DlgContents::on_listWidgetContents_itemClicked(QListWidgetItem* item)
{
	if (ui->btnUpdateList->text().indexOf(BUTTON_IN_PROGRESS) != -1)
		return;
	int currentRow = ui->listWidgetContents->row(item);
	if (currentRow >= 0)
	{
		m_listBookInfo[0].chapter = currentRow;
		m_listBookInfo[0].pos = 0;
		((DlgBookRead*)parentWidget())->UpdateChapter();
	}
}

void DlgContents::on_btnUpdateList_clicked()
{
	if (m_listBookInfo[0].IsWeb() && ui->btnUpdateList->text().indexOf(BUTTON_IN_PROGRESS) == -1)
	{
		ui->btnUpdateList->setText(UPDATING_LIST_STRING);
		std::thread([&]
			{
				sBookInfo book = m_listBookInfo[0];
				vector<BookChapterList> chapterList;
				int iRet = ThreadCacheBook::GetChapterList(book, chapterList, true);
				if (iRet >= 0 || book.path == m_listBookInfo[0].path)
				{
					((DlgBookRead*)parent())->m_chapterList = chapterList;
					//book.chapter = m_listBookInfo[0].chapter;
					//book.pos = m_listBookInfo[0].pos;
					m_listBookInfo[0] = book;
					emit on_sig_update_contents(true);
				}
				else
				{
					emit on_sig_update_contents(false);
				}
			}).detach();
	}
}

void DlgContents::on_slots_update_contents(bool b)
{
	if (b)
	{
		ListWidgetClear();
		UpdateContents();
		((DlgBookRead*)parentWidget())->UpdateChapter();
	}
	else
	{
		QMessageBox::warning((QWidget*)this, "警告", "更新列表错误");
		LOG_WRITE(LOG_LEVEL_WARN, "更新列表错误");
	}
	ui->btnUpdateList->setText(UPDATE_LIST_STRING);
}

void DlgContents::on_cbSort_stateChanged(int arg1)
{
	m_listBookInfo[0].sort = Qt::CheckState(arg1);
}

void DlgContents::reject()
{
	if (check_state_old != m_listBookInfo[0].sort)
	{
		SaveBookListInfo();
	}
	DlgCustom::reject();
}

void DlgContents::show()
{
	check_state_old = m_listBookInfo[0].sort;
	DlgCustom::show();
}
